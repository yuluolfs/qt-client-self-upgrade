#include "manage.h"
#include "input.h"

#include <QDebug>

Manage::Manage(QObject *parent)
    : QObject(parent)
    , server(nullptr)
    , connectSum(0)
    , key(0)
{

}

void Manage::init()
{
    server = new QTcpServer(this);
    if (!server->listen(QHostAddress::Any, 1102)) {
        qDebug() << "listen fail";
        return;
    }
    qDebug() << "listen succeed";
    connect(server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    input* p = new input();
    p->start();
    connect(p, SIGNAL(signalStartUpdate()), this, SLOT(onStartUpdate()));
}

void Manage::onNewConnection()
{
    connectSum++;
    key++;
    qDebug() << "connectSum " << connectSum;
    qDebug() << QThread::currentThreadId();
    processingThread* pT = new processingThread(server->nextPendingConnection(), key);
    QThread* t = new QThread();
    pT->moveToThread(t);
    socketList.insert(pT, t);
    connect(pT, SIGNAL(signalDisconnect(int)), this, SLOT(onDisconnect(int)));
    connect(this, SIGNAL(signalSendUpdateRequest()), pT, SLOT(onSendUpdateRequest()));
    connect(t, SIGNAL(started()), pT, SLOT(init()));
    connect(t, SIGNAL(finished()), pT, SLOT(deleteLater()));
    t->start();
}

void Manage::onDisconnect(int key)
{
    connectSum--;
    qDebug() << "connectSum " << connectSum;
    QMap<processingThread*, QThread*>::iterator i = socketList.begin();
    while (i != socketList.end()) {
        //find key
        if (i.key()->getKey() == key) {
            i.value()->exit();//推出线程
            i.value()->wait(10 * 1000);//设置等待,
            socketList.remove(i.key());
            break;
        }
    }
}

void Manage::onStartUpdate()
{
    qDebug() << "onStartUpdate";
    emit signalSendUpdateRequest();
}
