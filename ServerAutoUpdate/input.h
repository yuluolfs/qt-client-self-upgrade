#ifndef INPUT_H
#define INPUT_H

#include <QObject>
#include <QThread>

class input : public QThread
{
    Q_OBJECT
public:
    explicit input(QThread *parent = nullptr);

protected:
    void run();
signals:
    void signalStartUpdate();
};

#endif // INPUT_H
