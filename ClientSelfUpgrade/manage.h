#ifndef MANAGE_H
#define MANAGE_H

#include <QObject>
#include <QThread>

#include "clientupdate.h"

class Manage : public QObject
{
    Q_OBJECT
public:
    explicit Manage(QObject *parent = nullptr);
    void Minit();
signals:
    void signalInit();
private:
    ClientUpdate* cU;
};

#endif // MANAGE_H
