#ifndef MANAGE_H
#define MANAGE_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>

#include "processingthread.h"

class Manage : public QObject
{
    Q_OBJECT
public:
    explicit Manage(QObject *parent = nullptr);
    void init();
private slots:
    void onNewConnection();
    void onDisconnect(int key);
    void onStartUpdate();
signals:
    void signalSendUpdateRequest();
private:
    QMap<processingThread*, QThread*> socketList;

    QTcpServer* server;
    int connectSum;
    int key;
};

#endif // MANAGE_H
